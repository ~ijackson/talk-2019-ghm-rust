macro_rules! debug {
    ($g:expr, $($rhs:tt)*) => {
        (
            if debugp!($g) { eprint!($($rhs)*); }
        )
    }
}
