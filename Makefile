
# A
SLIDES+= intro

# B
SLIDES+= mm0
SLIDES+= mm
SLIDES+= borrow-mut
SLIDES+= dangling
SLIDES+= borrow-nonmut

# C
SLIDES+= tour-61
SLIDES+= tour-62
SLIDES+= tour-63
SLIDES+= tourx-71
SLIDES+= tourx-72

# D
SLIDES+= c-v-rust
SLIDES+= stability-slide
SLIDES+= borrow-errormsg
SLIDES+= coc-slide

# E
SLIDES+= bck-lifetimes
SLIDES+= macros
SLIDES+= cargo
SLIDES+= otherproblems

# F
SLIDES+= conclusion

SLIDEFILES=$(addsuffix .ps, $(SLIDES))

o= >$@.new && mv -f $@.new $@

all:	slides.pdf talk.ps

%.ps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4  <$@.1 $o

mm0.ps: mm.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D-60 <$@.1 $o

bck-lifetimes.ps: bck-err.txt.eps
macros.ps: serde-example.txt.eps macro-rules-example.txt.eps
borrow-mut.ps: borrow-mut-example.txt.eps
dangling.ps: dangling.txt.eps dangling-err.txt.eps
borrow-nonmut.ps borrow-errormsg.ps: borrow-nonmut.txt.eps borrow-nonmut-error.txt.eps
cargo.ps: cargo-nonconfig.txt.eps

tour-main.ps: tour-example.txt.eps

tour-%.ps: tour-main.fig tour-example.txt.eps Makefile
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+1:59,$*,80:99,300:999 <$@.1 $o

tourx-%.ps: tour-main.fig unsafe-example.txt.eps ffi-example.txt.eps ffi-example-cpp.txt.eps Makefile
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D+1:59,$*,80:89,91:99,300:999,1$*,2$* <$@.1 $o

%.eps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L eps <$@.1 $o

%.txt.eps: %.txt ./txt2ps
	./txt2ps <$< |ps2eps -s a3 $o

%.ps:	%.lout
	lout $< $o

slides.ps: $(SLIDEFILES) Makefile
	cat $(SLIDEFILES) $o

%.pdf:     %.ps Makefile
	ps2pdf $< $@

talk.ps: %.ps: %.txt Makefile
	a2ps -1 -f12.5 -o $@.1.tmp -B $<
	pstops <$@.1.tmp >$@ '0@0.94(7mm,7.5mm)'

usb=/media/usb1

for-printing: talk.pdf talk.ps
	mount $(usb)
	cp $^ $(usb)/.
	umount $(usb)

install: slides.pdf talk.txt
	rsync -vP $^ ijackson@chiark:public-html/2019/ghm-rust-talk/
	git push ijackson@chiark:public-git/talk-2019-ghm-rust.git
